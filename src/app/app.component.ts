import { Component, OnInit } from '@angular/core';
import { of, Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { IField, IToken } from './fields.model';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    public mockData$: Observable<IToken[]>;
    public form: FormGroup;
    public selectedToken: IToken;
    private data: IToken[];

    constructor(private formBuilder: FormBuilder) {
        this.mockData$ = of([
            {
                token: '001',
                fields: [
                    {
                        name: 'Ref1',
                        label: 'Lender',
                        mandatory: true,
                        fieldValues: null
                    },
                    {
                        name: 'Ref2',
                        label: '',
                        mandatory: true,
                        fieldValues: [
                            'zzup01', 'zzup02'
                        ]
                    },
                    {
                        name: 'Ref3',
                        label: 'Alder',
                        mandatory: false,
                        fieldValues: null
                    },
                    {
                        name: 'Ref4',
                        label: '',
                        mandatory: true,
                        fieldValues: 'UST-Global'
                    },
                    {
                        name: 'Ref5',
                        label: 'Last One',
                        mandatory: false,
                        fieldValues: 'UST-Global'
                    }
                ]
            },
            {
                token: '002',
                fields: [
                    {
                        name: 'Ref1',
                        label: '',
                        mandatory: false,
                        fieldValues: null
                    },
                    {
                        name: 'Ref2',
                        label: '',
                        mandatory: true,
                        fieldValues: 'Apple'
                    },
                    {
                        name: 'Ref3',
                        label: '',
                        mandatory: false,
                        fieldValues: null
                    }
                ]
            }
        ]);
        this.mockData$.subscribe((data: IToken[]) => {
            this.data = data;
            this.selectedToken = this.data[0];
        });
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            fields: this.formBuilder.array([])
        });
    }

    public updateForms(event) {
        const foundData = this.data.find(data => data.token === event.target.value);
        this.selectedToken = foundData;
        const group = this.formBuilder.array([]);
        if (foundData) {
            foundData.fields.forEach(control => {
                group.push(this.transformForm(control));
            });
        }
        this.form = this.formBuilder.group({
            fields: group
        });
    }

    public isText = (field: IField) => field.fieldValues === null || typeof field.fieldValues === 'string';

    private transformForm(control: IField) {
        const value = control.fieldValues ?  control.fieldValues : '';
        const required = control.mandatory ? Validators.required : null;
        return new FormControl(value, required);
    }

    get demoArray() {
        return this.form.get('fields') as FormArray;
    }
}
