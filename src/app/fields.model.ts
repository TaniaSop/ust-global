export interface IToken {
    token: string;
    fields: IField[];
}

export interface IField {
    name: string;
    label: string;
    mandatory: boolean;
    fieldValues: Array<string> | string | null;
}
